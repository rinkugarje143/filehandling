﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal interface IFiles
    {
        void WriteContentToFile(User user, string fileName);
        List<string> ReadContentFromFille(string fileName);

        bool IsUserNameExists(string userName, string fileName);
    }


}
